﻿// Copyright (c) Maarten Balliauw. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CamoDotNet.Core")]
[assembly: AssemblyDescription("CamoDotNet is all about making insecure assets look secure. Contains client-side code.")]
[assembly: Guid("8d7bff2e-d1b2-4814-9c3d-7bd1477c87cc")]

// Shared data
[assembly: AssemblyCompany("Maarten Balliauw")]
[assembly: AssemblyProduct("CamoDotNet")]
[assembly: AssemblyCopyright("Copyright © Maarten Balliauw")]
[assembly: AssemblyTrademark("Maarten Balliauw")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("3.0.0.0")]
[assembly: AssemblyFileVersion("3.0.0.0")]