﻿// Copyright (c) Maarten Balliauw. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CamoDotNet")]
[assembly: AssemblyDescription("CamoDotNet is all about making insecure assets look secure.")]
[assembly: Guid("b67c9276-b4d1-4c2a-a8ab-4383d3bb5d55")]

// Shared data
[assembly: AssemblyCompany("Maarten Balliauw")]
[assembly: AssemblyProduct("CamoDotNet")]
[assembly: AssemblyCopyright("Copyright © Maarten Balliauw")]
[assembly: AssemblyTrademark("Maarten Balliauw")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("3.0.0.0")]
[assembly: AssemblyFileVersion("3.0.0.0")]